<?php

use Faker\Generator as Faker;
use Brunoocto\Process\Models\Process;

$factory->define(Process::class, function (Faker $faker) {
    return [
        'pid' => $faker->unique()->numberBetween(10000, 99999),
        'server' => $faker->randomElement(['192.168.3.1:443 (server1)', '192.168.3.2:443 (server2)', '192.168.3.3:443 (server3)']),
        'script' => $faker->randomElement(['test.sh', 'doesnotexist.sh']),
        'output' => $faker->randomElement([true, false]),
        'timeout' => rand(2, 8),
        'parameters' => $faker->randomElement([
            null,
            json_encode(['a' => 1,]),
            json_encode([
                'a' => 2,
                'b' => ['c' => 3],
            ])
        ]),
    ];
});
