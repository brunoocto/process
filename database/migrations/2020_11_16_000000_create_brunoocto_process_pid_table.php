<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrunooctoProcessPidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brunoocto_processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps(3);
            $table->timestamp('launched_at', 3)->nullable()->comment('When the process started');
            $table->timestamp('expires_on', 3)->nullable()->comment('Expiration date of the process');
            $table->unsignedInteger('timeout')->default(60)->comment('Maximum running time in seconds');
            $table->unsignedInteger('pid')->nullable()->comment('Process PID');
            $table->string('server', 100)->nullable()->comment('Server where the process run');
            $table->string('script', 100)->comment('Script to run');
            $table->boolean('output')->default(false)->comment('Script output');
            $table->json('parameters')->nullable()->comment('Parameters to give the the script');

            $table->unique(['pid', 'server']);
            $table->index('server');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('brunoocto_processes');
    }
}
