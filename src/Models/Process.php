<?php

namespace Brunoocto\Process\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Process model
 *
 */
class Process extends Model
{
    /**
     * Timeout of a script
     * 60s by default
     *
     * @var integer
     */
    protected const TIMEOUT = 60;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'brunoocto_processes';

    /**
     * Guarded attributes
     * We disbale mass-assignement
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Return the Server identity where the process runs
     *
     * @return string
     */
    public static function serverID()
    {
        $server_addr = $_SERVER['SERVER_ADDR'] ?? request()->server('SERVER_ADDR');
        $hotname = $_SERVER['HOSTNAME'] ?? request()->server('HOSTNAME');
        return $server_addr.'['.$hotname.']';
    }

    /**
     * Kill processes
     * Kill linux processes and remove them from the database
     *
     * @return array Array of processes killed
     */
    public static function cleanExpired()
    {
        $ids = [];
        // Get all Processes that are launched and expired
        $processes = Process::whereNotNull('pid')->get(['id', 'expires_on', 'pid']);
        
        foreach ($processes as $process) {
            // We check if the process still exists, if not, we clean the record from the database
            $pid_name = exec('ps -p '.$process->pid.' -o comm=;');
            if (is_null($process->expires_on)) {
                if (empty($pid_name)) {
                    $ids[] = $process->id;
                    // Force to kill the process
                    exec('kill -9 '.$process->pid.' > /dev/null 2>&1 &');
                }
            } elseif ($process->expires_on <= now()->toDateTimeString()) {
                $ids[] = $process->id;
                // Force to kill the process
                exec('kill -9 '.$process->pid.' > /dev/null 2>&1 &');
            }
        }
        if (count($ids)) {
            // Bulk delete
            Process::destroy($ids);
        }
        return $ids;
    }

    /**
     * Get default Timeout
     *
     * @return integer
     */
    public static function getTimeout()
    {
        return static::TIMEOUT;
    }
    
    /**
     * Kill a process
     * Kill linux process and remove it from database
     *
     * @return boolean At true if delete succeed
     */
    public function kill()
    {
        // Force to kill the process
        exec('kill -9 '.$this->pid.' > /dev/null 2>&1 &');
        return $this->delete();
    }

    /**
     * Get default Timeout
     *
     * @return integer
     */
    public function toJsonApi()
    {
        $json = $this->toArray();

        // Delet ID from attributes
        unset($json['id']);
        
        // Format for vnd.api+json
        $json_api = (object)[
            'id' => $this->id,
            'type' => $this->getTable(),
            'attributes' => (object)$json,
        ];
        return $json_api;
    }

    /**
     * Record that a process started with its PID
     *
     * @param integer $pid PID of started shell process
     * @return integer
     */
    public function start(int $pid)
    {
        // Set offset timeout
        $timeout = static::getTimeout();
        if ($this->timeout >= 0) {
            $timeout = $this->timeout;
        }
        // Save to the database to track the process running
        $this->pid = $pid;
        $this->server = static::serverID();
        $this->launched_at = now();
        // At 0 or less, there is no timeout of the script
        if ($timeout <= 0) {
            $this->timeout = 0;
            $this->expires_on = null;
        } else {
            $this->expires_on = now()->addSecond($timeout);
        }
        return $this->save();
    }
}
