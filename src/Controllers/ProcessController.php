<?php

namespace Brunoocto\Process\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Brunoocto\Process\Models\Process;
use Brunoocto\Json\Contracts\JsonInterface;
use Brunoocto\Process\Contracts\PidInterface;
use Brunoocto\Filesystem\Services\FolderService;
use Brunoocto\Process\Rules\ArrayOrObject;

/**
 * GET putStartProcesses()
 * POST postLaunchProcess($script, $param = [], $limit = 10)
 * GET putShowProcesses($start_id = 0, $limit = false)
 * GET putShowProcess($id)
 * GET putCleanProcesses()
 * DELETE deleteKillProcess($id)
 * DELETE deleteKillProcesses()
 *
 */

class ProcessController extends Controller
{
    
    /**
     * Prepare a process(es) to run and place it in a queue to be launched in another thread.
     *
     * @param Illuminate\Http\Request $request
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @return Illuminate\Http\Response
     */
    public function postProcess(Request $request, JsonInterface $json)
    {
        $request->validate([
            'data' => ['required', new ArrayOrObject],
        ]);

        // Get table name
        $process_table_name = (new Process)->getTable();

        // Check data content if it's one object(object) or a collection(array)
        $data = $request->input('data');
        $items = [];
        if (array_key_exists(0, $data)) {
            // Collection
            foreach ($data as $key => $item) {
                $items[] = (object)$item;
                // Validate each object received
                $request->validate([
                    'data.'.strval($key).'.type' => 'required|in:'.$process_table_name,
                    'data.'.strval($key).'.attributes.script' => 'required|string|max:100',
                    'data.'.strval($key).'.attributes.output' => 'boolean',
                    'data.'.strval($key).'.attributes.timeout' => 'integer|min:0',
                ]);
            }
        } elseif (array_key_exists('type', $data)) {
            // Single object
            $items[] = (object)$data;
            // Validate the object received
            $request->validate([
                'data.type' => 'required|in:'.$process_table_name,
                'data.attributes.script' => 'required|string|max:100',
                'data.attributes.output' => 'boolean',
                'data.attributes.timeout' => 'integer|min:0',
            ]);
        }

        // We use meta if we need to inform the user about some failure
        $meta = null;
        // Loop each object
        $data = null;
        foreach ($items as $key => $item) {
            // Get attributes and make sure we work with object properties
            $attributes = (object)$item->attributes;
            
            $script = $attributes->script;
            // Avoid any return in the path
            $script = str_replace('..', '', $script);
            // Check if the script exists
            if (!is_file(resource_path('scripts/'.$script.'.sh'))) {
                $meta[$key][] = [
                    'error' => 'The script file "'.$script.'" does not exists.',
                    'data' => $item,
                ];
                continue;
            }
    
            $param = null;
            if (isset($attributes->parameters) && !empty($attributes->parameters)) {
                // Convert anything into an array to loop it
                $parameters = (array)$attributes->parameters;
                // We rebuild the array to keep only the 1st level
                $param = [];
                foreach ($parameters as $parameter) {
                    if (!is_string($parameter)) {
                        $parameter = json_encode($parameter);
                    }
                    $param[] = $parameter;
                }
                $param = json_encode($param);
            }
    
            // We record the process into the database
            $process = new Process;
            $process->script = $script;
            if (isset($attributes->output)) {
                $process->output = (boolean)$attributes->output;
            }
            if (isset($attributes->timeout)) {
                $process->timeout = (int)$attributes->timeout;
            }
            // Parameters will be escaped later while the exec will be called for security purpose
            $process->parameters = $param;
            
            if ($process->save()) {
                // This line helps to get all properties recorded onto the database
                $process = Process::find($process->id);
                $data[] = $process->toJsonApi();
            }
        }
        
        // If we succeed to record at least one entry
        if (is_array($data)) {
            return $json->send($data, 200, 'Process queued', $meta);
        }
        
        // If no any object recorded
        return $json->error(400, '', $meta);
    }


    /**
     * Start to loop to launch all scripts in queue.
     * This script will stopped after a certain number of iterations, it has to be reactivated via a crontab or API call. Ideally every one hour. The script itself should run about 24H, but calling start again won't duplicate it.
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @param Brunoocto\Process\Contracts\PidInterface $pid_service Service to work with running PHP thread
     * @return Illuminate\Http\Response
     */
    public function putStartProcesses(JsonInterface $json, PidInterface $pid_service)
    {
        if ($pid_service->isAlone() == false) {
            return $json->send(null, 200, 'Already running, no action done.');
        }

        // Force (but do not stop) immediate feedback to the client
        $json->send(null, 200, 'Processes started');

        // Run in medium priority
        proc_nice(5);
        
        // Clean current working PHP process if any to make sure we only run one PHP process per application instance.
        $pid_service->cleanPID();
        // We keep track of the running PHP process
        $pid_service->savePID();

        // Prepare the logs' directory
        $filesystem = new FolderService();
        $filesystem->setPath($pid_service->getOutputPath(), true);
        
        $limit = (int)env('LINCKO_PROCESS_NUMBER_ITERATIONS');
        // Infinite loop
        while (env('LINCKO_PROCESS_INFINITE_ITERATION') || $limit > 0) {
            $limit--;

            // Process timeout in 60s to assure the continuity of the PHP process
            set_time_limit(60);
            
            // Sleep time in micro-seconds
            $usleep = 300000; //300 ms

            // Clean all expired processes
            Process::cleanExpired();

            // Check if we exceed running processes (default at 4) on current server
            $count = Process::whereNotNull('launched_at')->where('server', Process::serverID())->limit(intval(env('LINCKO_PROCESS_PARALLEL'))+1)->count();
            if ($count < intval(env('LINCKO_PROCESS_PARALLEL'))) {
                // This call will refresh the modification date of the file
                $pid_service->savePID();

                // We find and launch the first process
                if ($process = Process::whereNull('launched_at')->first()) {
                    
                    // Accelerate scripts launch between 2 scripts
                    $usleep = 100000; //100 ms

                    // If the script does not exists, we delete it
                    $script = $process->script;
                    // Avoid any return in the path (but should not happen here expect if the database has been modified)
                    $script = str_replace('..', '', $script);
                    // Build the realpath
                    $script = resource_path('scripts/'.$script.'.sh');
                    // Check if the script exists
                    if (!is_file($script)) {
                        $process->delete();
                        continue;
                    }

                    // Build the arguments attached to the script
                    $cmd_parameters = '';
                    $parameters = $process->parameters;
                    if ($parameters) {
                        $parameters = json_decode($parameters);
                        // Protect the command from injection attack
                        $parameters = array_map(function ($value) {
                            return '"'.addslashes($value).'"';
                        }, $parameters);
                        // Implode the array to make them as arguments
                        if (is_array($parameters)) {
                            $cmd_parameters = implode(' ', $parameters);
                        }
                    }

                    // Clean the output to get the new exec PID
                    unset($output);

                    // Do not keep logs by default
                    $log = '/dev/null';
                    if ($process->output && env('LINCKO_PROCESS_OUTPUT')) {
                        $log = $pid_service->getOutputPath().$process->id.'.log';
                    }

                    // This script insure to write the command on one single line which is requested to be able to skip the wait of the script execution
                    $cmd = 'bash "'.$script.'" '.$cmd_parameters.' > "'.$log.'" 2>&1 & echo $!';

                    // Launch the script in background
                    exec($cmd, $output, $code);
                    
                    // Get script PID
                    $pid = null;
                    if (array_key_exists(0, $output) && is_numeric($output[0]) && $output[0] > 0) {
                        $pid = (int)$output[0];
                    }
                    
                    // "0" means the execution of the script is running
                    if ($code == 0) {
                        $process->start($pid);
                        // Display for phunit only
                        if (env('APP_ENV') == 'testing' && env('APP_DEBUG')) {
                            dump("[$pid] => ".now()->toString());
                        }
                    } else {
                        // Force to kill the process if anything wrong
                        exec('kill -9 '.$pid.' > /dev/null 2>&1 &');
                    }
                }
            }

            // Pause between each process to eventually give hand to other queueing task
            usleep($usleep);
        }

        return $json->send(null, 200, 'Processes stopped');
    }

    /**
     * It stops the main PHP process to not start next processes in the queue
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @param Brunoocto\Process\Contracts\PidInterface $pid_service Service to work with running
     * @return Response
     */
    public function putStopProcesses(JsonInterface $json, PidInterface $pid_service)
    {
        // Clean current working PHP process if any to make sure we only run one PHP process per application instance
        $data = $pid_service->cleanPID(false);

        return $json->send($data, 200, 'Processes stopped');
    }
    
    /**
     * List all processes regardless theier status
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @return Response
     */
    public function getProcesses(JsonInterface $json, $start_id = 0, $limit = false)
    {
        $processes = [];
        $items = Process::all();
        foreach ($items as $item) {
            $processes[] = $item->toJsonApi();
        }
        return $json->send($processes, 200, 'Processes list');
    }

    /**
     * Get a Process
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @return Response
     */
    public function getProcess(JsonInterface $json, int $id)
    {
        $processes = [];
        if ($process = Process::find($id)) {
            $processes[] = $process->toJsonApi();
        }
        return $json->send($processes, 200, 'Process '.$id);
    }

    /**
     * Clean finished or expired processes
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @return Response
     */
    public function putCleanProcesses(JsonInterface $json)
    {
        // Clean all expired processes
        $data = Process::cleanExpired();
        return $json->send($data, 200, 'Processes cleaned');
    }

    /**
     * Force to Kill all processes, even running ones that are not expired yet
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @return Response
     */
    public function deleteProcesses(JsonInterface $json)
    {
        $ids = [];
        $processes = Process::all();
        foreach ($processes as $process) {
            if ($process->kill()) {
                $ids[] = $process->id;
            }
        }
        return $json->send($ids, 200, 'All processes killed and removed');
    }

    /**
     * Force to kill the process by its ID
     *
     * @param Brunoocto\Json\Contracts\JsonInterface $json
     * @return Response
     */
    public function deleteProcess(JsonInterface $json, int $id)
    {
        $ids = [];
        if ($process = Process::find($id)) {
            if ($process->kill()) {
                $ids[] = $id;
                return $json->send($ids, 200, 'Process '.$id.' killed and removed');
            }
        }
        return $json->send($ids, 200, 'No process found');
    }
}
