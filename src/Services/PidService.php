<?php

namespace Brunoocto\Process\Services;

use Storage;
use DateTime;
use Brunoocto\Process\Contracts\PidInterface;
use Brunoocto\Filesystem\Services\FolderService;

/**
 * Manage PID of current PHP process
 *
 */
class PidService implements PidInterface
{
    /**
     * Path where PID files and logs will be saved
     */
    protected const PATH = 'brunoocto/process/';

    /**
     * Time limit to tell if the process is dead
     */
    protected const EXPIRATION_TIME = 10;

    /**
     * Consructor
     *
     * @return integer
     */
    public function __construct()
    {
        $folder = new FolderService();
        $folder->createPath($this->getPath(), 0750);
    }
    
    /**
     * Get PHP's process ID
     *
     * @return integer
     */
    public function getPID()
    {
        return getmypid();
    }

    /**
     * Get Time limit before a process is considered as dead
     *
     * @return integer
     */
    public function getExpirationTime()
    {
        return static::EXPIRATION_TIME;
    }

    /**
     * Get PID file path
     *
     * @return integer
     */
    public function getPath()
    {
        return storage_path(static::PATH);
    }

    /**
     * Get Process logs path
     *
     * @return integer
     */
    public function getOutputPath()
    {
        return $this->getPath().'output/';
    }

    /**
     * Save PHP's process ID
     *
     * @return boolean
     */
    public function savePID()
    {
        // Create a PID file with the human-redable UTC date (not used for any logic) when it starts
        return file_put_contents($this->getPath().$this->getPID().'.pid', DateTime::createFromFormat('U.u', microtime(true))->format('m-d-Y H:i:s.u'));
    }

    /**
     * Check if any other main process is running in parallel.
     *
     *
     * @return boolean At true there is only 1 main process (the current one) running
     */
    public function isAlone()
    {
        $alone = true;
        // Get all files in the folder
        //$files = Storage::disk('local')->allFiles($this->getPath());
        $filesystem = new FolderService(($this->getPath()));
        $files = $filesystem->loopFolder();
        if (array_key_exists('file', $files)) {
            foreach ($files['file'] as $file) {
                // Check only PID files
                if (preg_match('/(\d+)\.pid/ui', $file, $matches)) {
                    $pid = $matches[1];
                    // Exclude current process
                    if ($pid == $this->getPID()) {
                        continue;
                    }
                    // Check if the PID is a php running process, and if it's a recent file updated (less than 10s)
                    // Note that we are talking about the main (while loop) PHP process which launch each shell script, not shell scripts themselves.
                    $pid_name = exec('ps -p '.$pid.' -o comm=;');
                    if (strpos($pid_name, 'php') !== false && filemtime($file) > (now()->timestamp - $this->getExpirationTime())) {
                        // Another main process is running
                        $alone = false;
                        break;
                    }
                }
            }
        }
        return $alone;
    }

    /**
     * Kill working PHP processes about Process package only
     * Note that we are killing only the main PHP processes launched by (getStartProcesses), not the Shell scripts themselves
     *
     * @param bool $exclude_current At true it excludes the current process (NOTE: At false it will be killed!), default is true
     * @return array Array of PID killed
     */
    public function cleanPID($exclude_current = true)
    {
        $pids = [];
        // Get all files in the folder
        $filesystem = new FolderService(($this->getPath()));
        $files = $filesystem->loopFolder();
        if (array_key_exists('file', $files)) {
            foreach ($files['file'] as $file) {
                // Check only PID files
                if (preg_match('/(\d+)\.pid/ui', $file, $matches)) {
                    $pid = $matches[1];
                    // Exclude current process
                    if ($pid == $this->getPID() && $exclude_current) {
                        continue;
                    }
                    // Check if the PID is a php running process.
                    // Note that we are talking about the main (while loop) PHP process which launch each shell script, not shell scripts themselves.
                    $pid_name = exec('ps -p '.$pid.' -o comm=;');
                    if (strpos($pid_name, 'php') !== false) {
                        exec('kill -9 '.$pid.' > /dev/null 2>&1 &');
                        $pids[] = $pid;
                    }
                    // Delete PID file
                    @unlink($this->getPath().$pid.'.pid');
                }
            }
        }
        return $pids;
    }
}
