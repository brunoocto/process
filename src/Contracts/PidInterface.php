<?php

namespace Brunoocto\Process\Contracts;

interface PidInterface
{
    public function getPID();

    public function getOutputPath();

    public function savePID();

    public function cleanPID();
}
