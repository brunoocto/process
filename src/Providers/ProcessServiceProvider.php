<?php

namespace Brunoocto\Process\Providers;

use Dotenv\Dotenv;
use Illuminate\Support\ServiceProvider;
use Brunoocto\Process\Services\PidService;
use Brunoocto\Process\Contracts\PidInterface;

class ProcessServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Load environment variables specific to the library (default is .env)
        $dotenv = Dotenv::createMutable(__DIR__.'/../../');
        $dotenv->load();
        
        // Bind Interface
        $this->app->singleton(PidInterface::class, PidService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load routes into the framework
        $this->loadRoutesFrom(__DIR__.'/../Routes/ProcessRoutes.php');

        // Generate tables
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    }
}
