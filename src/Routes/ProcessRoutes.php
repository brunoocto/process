<?php

\Route::group(
    [
        'middleware' => ['api',],
        'prefix' => 'brunoocto/process',
        'namespace' => 'Brunoocto\Process\Controllers',
    ],
    function () {
        \Route::put('start', 'ProcessController@putStartProcesses');
        \Route::put('stop', 'ProcessController@putStopProcesses');
        // Mdoel Process
        \Route::group(
            [
                'prefix' => 'processes',
            ],
            function () {
                \Route::get('', 'ProcessController@getProcesses');
                \Route::get('{id}', 'ProcessController@getProcess')->where(['id' => '\d+']);
                \Route::post('', 'ProcessController@postProcess');
                \Route::delete('{id}', 'ProcessController@deleteProcess')->where(['id' => '\d+']);
                \Route::put('clean', 'ProcessController@putCleanProcesses');
                \Route::put('delete', 'ProcessController@deleteProcesses');
            }
        );
    }
);
