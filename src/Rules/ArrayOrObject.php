<?php

namespace Brunoocto\Process\Rules;

use Illuminate\Contracts\Validation\Rule;

class ArrayOrObject implements Rule
{
    /**
     * Determine if the parameter is an Array or an Object
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return is_array($value) || is_object($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must an Object or an Array of objects.';
    }
}
