#!/bin/bash
# suod apt-get install stress
# Stress by default during 4s using 1 CPU

# !!! IMPORTANT - SECURITY CONCERN !!!
# Script parameters must be protected too against malicious injection.

# default: 4
TIMEOUT="${1:-4}"
if ! [[ ${TIMEOUT} =~ ^[0-9]+$ ]]
then
   echo "error: TIMEOUT parameter is not a number"
   exit 1
fi

# default: 1
CPU="${2:-1}"
if ! [[ ${CPU} =~ ^[0-9]+$ ]]
then
   echo "error: CPU parameter is not a number"
   exit 1
fi

echo "start ${CPU} CPU ${TIMEOUT}s"
stress --timeout ${TIMEOUT} --cpu ${CPU}
echo "end"
