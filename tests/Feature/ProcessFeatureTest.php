<?php

namespace Brunoocto\Process\Tests\Feature;

use Brunoocto\Process\Models\Process;
use Brunoocto\Process\Tests\TestCase;
use Brunoocto\Process\Services\PidService;
use Brunoocto\Filesystem\Services\FolderService;

class ProcessFeatureTest extends TestCase
{

    /**
     * Initialize environement variable for test
     *
     * @return void
     */
    protected function environmentInitialization()
    {
        // Limit the number of processes checked
        $_ENV['LINCKO_PROCESS_INFINITE_ITERATION'] = $_SERVER['LINCKO_PROCESS_INFINITE_ITERATION'] = false;
        putenv('LINCKO_PROCESS_INFINITE_ITERATION=false');
        $_ENV['LINCKO_PROCESS_NUMBER_ITERATIONS'] = $_SERVER['LINCKO_PROCESS_NUMBER_ITERATIONS'] = 60;
        putenv('LINCKO_PROCESS_NUMBER_ITERATIONS=60');
    }

    /**
     * Get an available PID
     *
     * @return void
     */
    protected function startProcess()
    {
        // Create a fake running php process to get it's id
        $cmd = 'php -r "sleep(10);" > "/dev/null" 2>&1 & echo $!';
        exec($cmd, $output);
        
        // Get PID number
        $pid = getmypid() + rand(1, 9999);
        if (array_key_exists(0, $output) && is_numeric($output[0]) && $output[0] > 0) {
            $pid = (int)$output[0];
        }
        return $pid;
    }

    /**
     * Make sure that we do not kill current process
     *
     * @return void
     */
    protected function keepAlive()
    {
        // Delete PID file
        $pid_service = new PidService;
        @unlink($pid_service->getPath().$pid_service->getPID().'.pid');
    }

    

    /**
     * Test the queue of Processes
     * We do run more processes thant the application can accept to force the use of queueing process.
     *
     * @return void
     */
    public function testQueuedProcesses()
    {
        // Environment initialization
        $this->environmentInitialization();

        // We force to log scripts output
        $_ENV['LINCKO_PROCESS_OUTPUT'] = $_SERVER['LINCKO_PROCESS_OUTPUT'] = true;
        putenv('LINCKO_PROCESS_OUTPUT=true');

        // Create a temporary folder to store log files
        // VFS (virtual disk) cannot be used since it does not exists in the context of a shell script, only in current PHP process
        $filesystem = new FolderService('/tmp/brunoocto/process/output', true);
        
        // Mock PidService's method
        $this->partialMock(PidService::class, function ($mock) use ($filesystem) {
            $mock->shouldReceive('getOutputPath')->andReturn($filesystem->getPath());
        });

        // Create twice processes as the application can accept
        for ($i=0; $i < env('LINCKO_PROCESS_PARALLEL')*2; $i++) {
            // Create a process to queue
            $response = $this->json(
                'POST',
                '/brunoocto/process/processes',
                [
                    'data' => [
                        'type' => 'brunoocto_processes',
                        'attributes' => [
                            'script' => 'test', // Name of the script
                            'output' => true,
                            'timeout' => rand(2, 12), // Seconds
                            'parameters' => [
                                20, // (do not modify it) Stress Timeout
                                1, // (do not modify it) Stress number of CPU
                                'a' => [
                                    'b' => 'An array'
                                ],
                                'c' => 'A key/value',
                                'A value',
                                1234,
                                null,
                                false,
                                true,
                                "return; \n; line;",
                                (object)['key' => '你好 été лето'],
                                // The command passed as parameter should be parsed and treated as a string
                                'cmd' => '; echo "something"; sleep 10;',
                            ],
                        ],
                    ],
                ],
                [
                    'Content-Type' => 'application/vnd.api+json',
                    'Accept' => 'application/vnd.api+json',
                ]
            );
            // Check the Status (2xx)
            $response->assertSuccessful();
        }

        // Check the Body structure
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'message',
            ],
        ]);

        // Check if the json contains some value
        $response->assertJson([
            'data' => [
                [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'test',
                        'output' => true,
                    ],
                ],
            ],
        ]);

        // Check that the total number of processes registered is correct
        $count = Process::count();
        $this->assertEquals($count, env('LINCKO_PROCESS_PARALLEL')*2);
        
        // Keep current process alive
        $this->keepAlive();

        // Keep id of the first process to check log content
        $id = Process::first(['id'])->id;

        // Line return (only for a cleaner console output)
        dump('');

        // Start processes in parallel
        $response = $this->json(
            'PUT',
            '/brunoocto/process/start',
            [],
            ['Content-Type' => 'application/json',]
        );

        // Line return (only for a cleaner console output)
        dump('');

        // Check the Status (2xx)
        $response->assertSuccessful();

        // We stop logging outputs
        $_ENV['LINCKO_PROCESS_OUTPUT'] = $_SERVER['LINCKO_PROCESS_OUTPUT'] = false;
        putenv('LINCKO_PROCESS_OUTPUT=false');

        // Check the log content to confirm if the process properly run
        $this->assertStringContainsStringIgnoringCase('stress: info:', file_get_contents($filesystem->getPath().$id.'.log'));

        // We remove logs
        $filesystem->recursiveRemove();
    }


    /**
     * Test a process creation with minimum parameters
     *
     * @return void
     */
    public function testProcessWithMinimumParameters()
    {
        // Environment initialization
        $this->environmentInitialization();
        
        // Create Process
        $response = $this->json(
            'POST',
            '/brunoocto/process/processes',
            [
                'data' => [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'test',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
            ]
        );
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check if the json contains some value
        $response->assertJson([
            'data' => [
                [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'test',
                    ],
                ],
            ],
        ]);

        $pid = Process::first(['id'])->id;

        // Get Process
        $response = $this->json(
            'GET',
            '/brunoocto/process/processes/'.$pid
        );
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check if the json contains some value
        $response->assertJson([
            'data' => [
                [
                    'type' => 'brunoocto_processes',
                    'id' => $pid,
                ],
            ],
        ]);
    }


    /**
     * Test a process creation with minimum parameters
     *
     * @return void
     */
    public function testNoTimeLimitProcess()
    {
        // Environment initialization
        $this->environmentInitialization();

        
        $response = $this->json(
            'POST',
            '/brunoocto/process/processes',
            [
                'data' => [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'test',
                        'timeout' => 0,
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
            ]
        );
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check if the json contains some value
        $response->assertJson([
            'data' => [
                [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'test',
                        'timeout' => 0,
                    ],
                ],
            ],
        ]);
    }


    /**
     * Test a process creation with minimum parameters
     *
     * @return void
     */
    public function testCleanMultipleProcesses()
    {
        // Environment initialization
        $this->environmentInitialization();

        
        $response = $this->json(
            'POST',
            '/brunoocto/process/processes',
            [
                'data' => [
                    [
                        'type' => 'brunoocto_processes',
                        'attributes' => [
                            'script' => 'test',
                            'timeout' => 0,
                        ],
                    ],
                    [
                        'type' => 'brunoocto_processes',
                        'attributes' => [
                            'script' => 'test',
                            'timeout' => 1,
                        ],
                    ],
                    [
                        'type' => 'brunoocto_processes',
                        'attributes' => [
                            'script' => 'test',
                            'timeout' => 0,
                        ],
                    ],
                    [
                        'type' => 'brunoocto_processes',
                        'attributes' => [
                            'script' => 'test',
                            'timeout' => 2,
                        ],
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
            ]
        );
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check that the total number of processes registered is correct
        $count = Process::count();
        $this->assertEquals($count, 4);

        // Get all registered processes
        $response = $this->json(
            'GET',
            '/brunoocto/process/processes'
        );

        // Check the Status (2xx)
        $response->assertSuccessful();
        
        // Check that we have 2 processes
        $count = count(json_decode($response->content(), true)['data']);
        $this->assertEquals($count, 4);

        // Kill one process
        $id = Process::first(['id'])->id;
        $response = $this->json(
            'DELETE',
            '/brunoocto/process/processes/'.$id,
            [],
            ['Content-Type' => 'application/json',]
        );
        
        // Check that data contains the id of the process deleted
        $this->assertEquals(json_decode($response->content(), true)['data'][0], 1);

        // Check that the total number of processes registered is correct
        $count = Process::count();
        $this->assertEquals($count, 3);

        // Simulate start of Process number 3
        Process::find(3)->start($this->startProcess());
        // Simulate start of Process number 4
        Process::find(4)->start($this->startProcess());
        
        sleep(3);
        
        // Clean processes
        $response = $this->json(
            'PUT',
            '/brunoocto/process/processes/clean',
            [],
            ['Content-Type' => 'application/json',]
        );
        
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check that the total number of processes registered is correct
        // It should be only the 2 unlimited processes
        $count = Process::count();
        $this->assertEquals($count, 2);

        // Kill processes
        $response = $this->json(
            'PUT',
            '/brunoocto/process/processes/delete',
            [],
            ['Content-Type' => 'application/json',]
        );

        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check if the json contains the 2 remaining processes
        $response->assertJson([
            'data' => [2, 3],
        ]);

        // Check that the total number of processes registered is correct
        $count = Process::count();
        $this->assertEquals($count, 0);
    }

    /**
     * Test a process creation with minimum parameters
     *
     * @return void
     */
    public function testFormatError()
    {
        // Environment initialization
        $this->environmentInitialization();
        
        $response = $this->json(
            'POST',
            '/brunoocto/process/processes',
            [
                'data' => 123,
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
            ]
        );

        // Check that we receive an error status
        $this->assertGreaterThanOrEqual(400, $response->status());
        $this->assertLessThan(500, $response->status());
    }


    /**
     * Test a script that does not exists
     *
     * @return void
     */
    public function testMissingScript()
    {
        // Environment initialization
        $this->environmentInitialization();

        // Try to create a process with a script that does not exists
        $response = $this->json(
            'POST',
            '/brunoocto/process/processes',
            [
                'data' => [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'doesnotexist',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
            ]
        );
        
        // Check that we receive an error status
        $this->assertGreaterThanOrEqual(400, $response->status());
        $this->assertLessThan(500, $response->status());

        // Create a Process with a script that exists
        $response = $this->json(
            'POST',
            '/brunoocto/process/processes',
            [
                'data' => [
                    'type' => 'brunoocto_processes',
                    'attributes' => [
                        'script' => 'test',
                        'timeout' => 3,
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
            ]
        );
        
        // Check that we receive an error status
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Simulate that a script has been removed or renamed.
        $process = Process::first();
        $process->script = 'doesnotexist';
        $process->save();

        // Keep current process alive
        $this->keepAlive();
        
        // Start processes in parallel
        $response = $this->json(
            'PUT',
            '/brunoocto/process/start',
            [],
            ['Content-Type' => 'application/json',]
        );
        
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check that the total number of processes registered is correct
        $count = Process::count();
        $this->assertEquals($count, 0);
    }

    /**
     * Test to get a process that does not exists
     *
     * @return void
     */
    public function testGetUnknownProcess()
    {
        // Environment initialization
        $this->environmentInitialization();

        // No process created here, so it will fail
        $response = $this->json(
            'GET',
            '/brunoocto/process/processes/1'
        );

        // Check the Status (2xx)
        $response->assertSuccessful();
    }


    /**
     * Test to kill a process that does not exists
     *
     * @return void
     */
    public function testKillUnknownProcess()
    {
        // Environment initialization
        $this->environmentInitialization();

        // No process created here, so it will fail
        $response = $this->json(
            'DELETE',
            '/brunoocto/process/processes/1',
            [],
            ['Content-Type' => 'application/json',]
        );

        // Check the Status (2xx)
        $response->assertSuccessful();
    }


    /**
     * Test to stop parallel processes
     *
     * @return void
     */
    public function testStopProcesses()
    {
        // Environment initialization
        $this->environmentInitialization();

        // Keep current process alive
        $this->keepAlive();

        // No process created here, so it will fail
        $response = $this->json(
            'PUT',
            '/brunoocto/process/stop',
            [],
            ['Content-Type' => 'application/json',]
        );

        // Check the Status (2xx)
        $response->assertSuccessful();
    }


    /**
     * Test if a main process is already running
     *
     * @return void
     */
    public function testIsAlone()
    {
        // Environment initialization
        $this->environmentInitialization();

        // Mock PidService's method
        $this->partialMock(PidService::class, function ($mock) {
            $mock->shouldReceive('isAlone')->andReturn(false);
        });

        // Keep current process alive
        $this->keepAlive();

        // No process created here, so it will fail
        $response = $this->json(
            'PUT',
            '/brunoocto/process/start',
            [],
            ['Content-Type' => 'application/json',]
        );

        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check the Body structure
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'message',
            ],
        ]);
        
        $this->assertStringContainsStringIgnoringCase('Already', json_decode($response->content(), true)['meta']['message']);
    }
}
