<?php

namespace Brunoocto\Process\Tests\Unit\Services;

use Brunoocto\Process\Tests\TestCase;
use Brunoocto\Process\Services\PidService;
use Brunoocto\Process\Models\Process;

class ProcessServicesTest extends TestCase
{
    /**
     * Test a PidService Path generation
     *
     * @return void
     */
    public function testPidServicePaths()
    {
        // Create PidService instance
        $pid_service = new PidService;
        $this->assertEquals($pid_service->getPath(), storage_path('brunoocto/process/'));
        $this->assertEquals($pid_service->getOutputPath(), storage_path('brunoocto/process/output/'));
    }

    /**
     * Test that we can kill a create php process
     *
     * @return void
     */
    public function testKillPhpProcess()
    {

        // Create a fake running php process to get it's id
        $cmd = 'php -r "sleep(2);" > "/dev/null" 2>&1 & echo $!';
        exec($cmd, $output);
        
        // Get PID number
        $pid = 0;
        if (array_key_exists(0, $output) && is_numeric($output[0]) && $output[0] > 0) {
            $pid = (int)$output[0];
        }

        // Create PidService mock instance
        $pid_service = $this->partialMock(PidService::class, function ($mock) use ($pid) {
            $mock->shouldReceive('getPID')->andReturn($pid);
        });
        // Launch the constructor because "partialMock" does not call it
        $pid_service->__construct();
        
        // Create PID file with the replace PHP process
        $pid_service->savePID();
        
        // Clean the other PHP running process
        $pids = $pid_service->cleanPID(false);
        
        $this->assertArrayHasKey(0, $pids);

        $this->assertEquals($pids[0], $pid);

        // Simulate a Ghost process with (we can reuse the just-killed pid to make sure it is empty)
        $process = factory(Process::class)->make();
        $process->timeout = 0;
        $process->start($pid);
        $ids = Process::cleanExpired();

        // Check that we killed 1 process (the ghost one)
        $this->assertEquals(count($ids), 1);
    }

    /**
     * Test that we can kill a create php process
     *
     * @return void
     */
    public function testisAlone()
    {
        $pid_service = new PidService;
        // Generate the .pid file
        $pid_service->savePID();

        // Make sure we only run 1 process
        $pid_service->cleanPID();

        // Check that it's the only process running
        $is_alone = $pid_service->isAlone();
        $this->assertTrue($is_alone);

        // Create a fake running php process to get it's id
        $cmd = 'php -r "sleep(1);" > "/dev/null" 2>&1 & echo $!';
        exec($cmd, $output);
        
        // Get PID number
        $pid = 0;
        if (array_key_exists(0, $output) && is_numeric($output[0]) && $output[0] > 0) {
            $pid = (int)$output[0];
        }

        // Copy the .pid file to simulate a second process
        copy($pid_service->getPath().$pid_service->getPID().'.pid', $pid_service->getPath().$pid.'.pid');

        // The process shouldn't be alone
        $is_alone = $pid_service->isAlone();
        $this->assertFalse($is_alone);
        
        sleep($pid_service->getExpirationTime() + 1);

        // The process should be alone because the .pid file is expired
        $is_alone = $pid_service->isAlone();
        $this->assertTrue($is_alone);
    }
}
