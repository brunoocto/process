<?php

namespace Brunoocto\Process\Tests;

use org\bovigo\vfs\vfsStream;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase as TestbenchCase;
use Brunoocto\Json\Providers\JsonServiceProvider;
use Illuminate\Filesystem\FilesystemServiceProvider;
use Brunoocto\Process\Providers\ProcessServiceProvider;
use Brunoocto\Exception\Providers\ExceptionServiceProvider;

/**
 * TestCase for Service Provider
 */
class TestCase extends TestbenchCase
{
    // Trait which does rollback to initial status any database modified during testing
    use RefreshDatabase;

    /**
     * Setup
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Load fake data to the Database
        $this->withFactories(__DIR__.'/../database/factories');
    }

    /**
     * Get package providers.
     * @param  Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            ProcessServiceProvider::class,
            JsonServiceProvider::class,
            FilesystemServiceProvider::class,
            ExceptionServiceProvider::class,
        ];
    }

    /**
      * Initialize environment
      * @return void
      */
    protected function getEnvironmentSetup($app)
    {
        // Configure Temporary Test database (note that it can be slow to run all tests within big project, but optimizations are possible)
        $app['config']->set('database.default', 'testdb');
        $app['config']->set('database.connections.testdb', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);

        // This help to find script files for tests
        $app->setBasePath(__DIR__ . '/..');

        // Initialize Virtual disk
        $app->singleton('vfs_stream', function () {
            return vfsStream::setup('vfs');
        });

        // Set Virtual Storage path
        vfsStream::newDirectory('storage', 0770)->at(app('vfs_stream'));
        $app->useStoragePath(vfsStream::url('vfs').'/storage');

        // By default we do not log script output
        $_ENV['LINCKO_PROCESS_OUTPUT'] = $_SERVER['LINCKO_PROCESS_OUTPUT'] = false;
        putenv('LINCKO_PROCESS_OUTPUT=false');
    }

    /**
      * Initialize Aliases
      * @param mixed $app
      * @return array
      */
    protected function getPackageAliases($app)
    {
        return [
              'LinckoJson' => 'Brunoocto\Json\Facades\JsonFacade',
          ];
    }
}
